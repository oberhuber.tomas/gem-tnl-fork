// Copyright (c) 2004-2023 Tomáš Oberhuber et al.
//
// This file is part of TNL - Template Numerical Library (https://tnl-project.org/)
//
// SPDX-License-Identifier: MIT

#pragma once

#include <TNL/Exceptions/BackendBadAlloc.h>
#include <TNL/Exceptions/BackendSupportMissing.h>
#include <TNL/Backend/Macros.h>
#include "Traits.h"

namespace TNL::Allocators {

/**
 * \brief Allocator for the CUDA device memory space.
 *
 * The allocation is done using the `cudaMalloc` function and the deallocation
 * is done using the `cudaFree` function.
 */
template< class T >
struct Cuda
{
   using value_type = T;
   using size_type = std::size_t;
   using difference_type = std::ptrdiff_t;

   Cuda() = default;
   Cuda( const Cuda& ) = default;
   Cuda( Cuda&& ) noexcept = default;

   Cuda&
   operator=( const Cuda& ) = default;
   Cuda&
   operator=( Cuda&& ) noexcept = default;

   template< class U >
   Cuda( const Cuda< U >& )
   {}

   template< class U >
   Cuda( Cuda< U >&& )
   {}

   template< class U >
   Cuda&
   operator=( const Cuda< U >& )
   {
      return *this;
   }

   template< class U >
   Cuda&
   operator=( Cuda< U >&& )
   {
      return *this;
   }

   [[nodiscard]] value_type*
   allocate( size_type n )
   {
#ifdef __CUDACC__
      value_type* result = nullptr;
      if( cudaMalloc( (void**) &result, n * sizeof( value_type ) ) != cudaSuccess )
         throw Exceptions::BackendBadAlloc();
      return result;
#else
      throw Exceptions::BackendSupportMissing();
#endif
   }

   void
   deallocate( value_type* ptr, size_type )
   {
#ifdef __CUDACC__
      TNL_BACKEND_SAFE_CALL( cudaFree( (void*) ptr ) );
#else
      throw Exceptions::BackendSupportMissing();
#endif
   }
};

template< class T1, class T2 >
[[nodiscard]] bool
operator==( const Cuda< T1 >&, const Cuda< T2 >& )
{
   return true;
}

template< class T1, class T2 >
[[nodiscard]] bool
operator!=( const Cuda< T1 >& lhs, const Cuda< T2 >& rhs )
{
   return ! ( lhs == rhs );
}

}  // namespace TNL::Allocators

namespace TNL {
template< class T >
struct allocates_host_accessible_data< Allocators::Cuda< T > > : public std::false_type
{};
}  // namespace TNL
